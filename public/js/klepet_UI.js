/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}

function dodajSlike(vhodnoBesedilo) {
  var regexp = new RegExp('^https?:\/\/.*\.(?:png|jpg|gif)$', 'gi');
  var tabelaSlike = [];
  var t = vhodnoBesedilo.split(' ');
  
  for (var k = 0; k < t.length; k++) {
    if (t[k].charAt(0) == '"') {
      var string = t[k].split('');
      string[0] = '';
      t[k] = string.join('');
    }
    
    var dolzina = t[k].length;
    
    if (t[k].charAt(dolzina - 1) == '"') {
      var string = t[k].split('');
      string[dolzina - 1] = '';
      t[k] = string.join('');
    }
  }
  
  for (var i = 0; i < t.length; i++) {
    if (t[i].match(regexp)) {
      tabelaSlike.push(t[i]);
    }
  }
  
  for (var j = 0; j < tabelaSlike.length; j++) {
    $('#sporocila').append('<img src="' + tabelaSlike[j] + '" style="width: 200px; margin-left: 20px" /><br />');
  }
}


/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  var jeSmesko = sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1;
  if (jeSmesko) {
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
    return divElementHtmlTekst(sporocilo);
  } else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);  
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}


/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSmeske(sporocilo);
  var sistemskoSporocilo;
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
      dodajSlike(sporocilo);
      $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    dodajSlike(sporocilo);
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* 
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}


var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";

var originalnaImena = [];
var nadimki = [];

/*global nadimek*/
function nadimek(ime, nadimek) {
  var ujemanje = false;
  for (var i = 0; i < originalnaImena.length; i++) {
    if (originalnaImena[i] == ime) {
      nadimki[i] = nadimek;
      ujemanje = true;
    }
  }
  
  if (ujemanje == false) {
    originalnaImena.push(ime);
    nadimki.push(nadimek);
  }
}

/*global zamenjava*/
function zamenjava(uporabnik, novoIme) {
  for (var i = 0; i < originalnaImena.length; i++) {
    if (originalnaImena[i] == uporabnik) {
      originalnaImena[i] = novoIme;
      break;
    }
  }
}

// Počakaj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    var uporabnik = sporocilo.besedilo.split(':')[0].split(' ')[0];
    var nadimek = uporabnik;
    
    for (var j = 0; j < originalnaImena.length; j++) {
      if (originalnaImena[j] == uporabnik) {
        nadimek = nadimki[j] + " (" + originalnaImena[j]  + ")";
        break;
      }
    }
    
    var message = sporocilo.besedilo.replace(uporabnik, nadimek);
    
    var novElement = divElementEnostavniTekst(message);
    $('#sporocila').append(novElement);
    dodajSlike(sporocilo.besedilo);
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  });
  
  socket.on('zamenjava', function (sporocilo) {
    var split = sporocilo.besedilo.split(' ');
    
    for (var i = 0; i < originalnaImena.length; i++) {
    if (originalnaImena[i] == split[0]) {
      originalnaImena[i] = split[1];
      break;
    }
  }
    
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    for (var i=0; i < uporabniki.length; i++) {
      var uporabnik = uporabniki[i];
      for (var j = 0; j < originalnaImena.length; j++) {
        if (originalnaImena[j] == uporabnik) {
          uporabnik = nadimki[j] + " (" + originalnaImena[j]  + ")";
          break;
        }
      }
      $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabnik));
    }
    
    // posilnjanje zasebnega sporocila s klikom na uporabnika
    $('#seznam-uporabnikov div').click(function() {
      var emoji = divElementHtmlTekst('&#9756;').text();
      var prvi = $(this).text();
      var naslovnik = $(this).text();
      if (naslovnik.indexOf('(') >= 0 && naslovnik.indexOf(')') >= 0) {
        naslovnik = naslovnik.match(/\(([^)]+)\)/)[1];
      }
      klepetApp.procesirajUkaz('/zasebno "' + naslovnik + '" "' + emoji + '"');
      if ($(this).text() != trenutniVzdevek)
      {
        $('#sporocila').append(divElementHtmlTekst('(zasebno za ' + prvi + '): ' + emoji));
        $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
      }
      $('#poslji-sporocilo').focus();
    });
  });
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});